﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL.Example.Models
{
    public class StudentCourse
    {
        [PrimaryKey]
        [ForeignKey("Courses", "Id")]
        public int Course_Id { get; set; }

        [PrimaryKey]
        [ForeignKey("Students", "Id")]
        public int Student_Id { get; set; }
    }
}
