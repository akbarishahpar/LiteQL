﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL.Example.Models
{
    public class Course
    {        
        [PrimaryKey]        
        [AutoIncrement]        
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
