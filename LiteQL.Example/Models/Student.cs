﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL.Example.Models
{
    public class Student
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }
        public DateTime BirthDate { get; set; }

        [ForeignKey("Teachers", "Id")]
        public int Master_Id { get; set; }
    }
}
