﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LiteQL;

namespace LiteQL.Example.Models
{
    public class AppDbContext : SQLiteDatabase
    {        
        public Table<Course> Courses { get; set; }
        public Table<Student> Students { get; set; }
        public Table<Teacher> Teachers { get; set; }
        public Table<StudentCourse> StudentCourses { get; set; }
        public Table<TeacherCourse> TeacherCourses { get; set; }        

        public AppDbContext(string FileName):base(FileName)
        {            
        }
    }
}
