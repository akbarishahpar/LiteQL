﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL.Example.Models
{
    public class TeacherCourse
    {
        [PrimaryKey]
        [ForeignKey("Courses", "Id")]
        public int Course_Id { get; set; }

        [PrimaryKey]
        [ForeignKey("Teachers", "Id")]
        public int Teacher_Id { get; set; }
    }
}
