﻿using LiteQL.Example.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL.Example
{
    class Program
    {
        static void Main(string[] args)
        {            
            var db = new AppDbContext("model.sqlite");

            var Name = db
                    .SelectFrom(db.Teachers)
                    .IsEqual(db.Teachers, q => $"2 * {new { q.Id }.Translate(db.Teachers)}".Complex(), 200)
                    .Take<Teacher>()
                    .First().Name;

            Console.WriteLine($"Name={Name}");            
            Console.ReadKey();
        }
    }
}
