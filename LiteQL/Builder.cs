﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace LiteQL
{
    public class Builder
    {
        public SQLiteConnection _connection;
        public Dictionary<string, Table> tables = new Dictionary<string, Table>();
        public SQLiteDatabase database;

        public Builder(SQLiteDatabase db, SQLiteConnection connection)
        {
            this._connection = connection;
            this.database = db;
        }
        public void Build()
        {
            foreach (var property in database.GetType().GetProperties())
            {
                if (property.PropertyType.BaseType == typeof(Table))
                {
                    var attr = property.GetCustomAttributes(typeof(TableName), true).Cast<TableName>().SingleOrDefault();
                    var tableName = attr?.Name;
                    if (string.IsNullOrEmpty(tableName))
                    {
                        tableName = property.Name;
                    }

                    var table = Activator.CreateInstance(property.PropertyType) as Table;
                    table._builder = this;
                    table._tableName = tableName;
                    table._tableType = property.PropertyType.GetGenericArguments()[0];
                    table.InitAttributes();
                    property.SetValue(this.database, table);
                    tables.Add(tableName, table);
                }
            }

            foreach (KeyValuePair<string, Table> item in tables)
            {
                var type = database.GetType();
                
                item.Value.Configure();

                var properties = type.GetProperties();
                foreach (var property in properties)
                {
                    if (tables.ContainsKey(property.Name))
                    {
                        var newType = Convert.ChangeType(tables[property.Name], property.PropertyType);
                        property.SetValue(database, newType);
                    }
                }
            }
        }
    }
}
