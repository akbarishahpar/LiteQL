﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace LiteQL
{
    public class SQLiteDatabase
    {        
        public Builder builder;

        public SQLiteDatabase(string fileName)
        {
            builder = new Builder(this, new SQLiteConnection($"DataSource={fileName}"));
            Configure();
        }

        public virtual string GetSqlDataType(Type t)
        {
            if (t == typeof(string) ||
                t == typeof(DateTime?) ||
                t == typeof(DateTime))
                return "TEXT";
            else if (
                    t == typeof(int) ||
                    t == typeof(int?) ||
                    t == typeof(long) ||
                    t == typeof(long?))
                return "INTEGER";

            else if (
                    t == typeof(double) ||
                    t == typeof(double?))
                return "REAL";

            else throw new Exception("Undefined Type");
        }                

        protected virtual void Configure()
        {            
            builder.Build();
        }

        public IEnumerable<T> Select<T>(string query)
        {            
            return builder._connection.Query<T>(query);            
        }

        public SelectQuery SelectFrom<T>(Table<T> table, Func<T, object> func = null, string As = null)
        {
            var sq = new SelectQuery().SelectFrom(table, func, As);
            sq.db = this;
            return sq;
        }
    }

    public class SelectQuery
    {
        public SQLiteDatabase db { get; set; }
        List<string> fromTables = new List<string>();
        List<string> selectedColumns = new List<string>();
        List<string> orderByColumns = new List<string>();
        List<string> groupedByColumns = new List<string>();
        List<List<Condition>> conditions = new List<List<Condition>>();
        string limit;
        string offset;

        string orderDirection;

        public SelectQuery SelectFrom<T>(Table<T> table, Func<T, object> func = null, string As = null)
        {
            var columnBase = string.IsNullOrEmpty(As) ? table._tableName : As;

            var fromName = table._tableName;
            if (!string.IsNullOrEmpty(As))
                fromName = $"{fromName} AS {As}";

            if(fromTables.All(q => q != fromName))
                fromTables.Add(fromName);

            if (func == null) selectedColumns.Add($"{columnBase}.*");            
            else
            {
                var obj = func(Activator.CreateInstance<T>());
                if (obj.GetType() == typeof(Aggregation))
                {
                    var agg = obj as Aggregation;
                    agg.baseFromTable = fromName;
                    if (string.IsNullOrEmpty(agg.secondaryFromTable)) agg.secondaryFromTable = agg.baseFromTable;

                    selectedColumns.Add($"{agg}");
                }
                else if(obj.GetType() == typeof(Complex))
                {
                    selectedColumns.Add(obj.ToString());
                }
                else
                {
                    var properties = obj.GetType().GetProperties();
                    foreach (var property in properties)
                    {
                        selectedColumns.Add($"{columnBase}.{property.Name}");
                    }
                }
            }
            return this;
        }

        public SelectQuery OrderBy<T>(Table<T> table, Func<T, object> func, string As = null, bool descending = false)
        {
            var fromTable = string.IsNullOrEmpty(As) ? table._tableName : As;


            var funcResult = func.Invoke(Activator.CreateInstance<T>());
            string orderby;

            if (funcResult?.GetType() == typeof(Complex))
            {
                var comp = (funcResult as Complex);
                orderby = comp.ToString();
                orderByColumns.Add(orderby);
            }
            else if (funcResult?.GetType() == typeof(Aggregation))
            {
                var agg = (funcResult as Aggregation);
                agg.baseFromTable = fromTable;
                orderby = agg.ToString();
                orderByColumns.Add(orderby);
            }
            else if (funcResult?.IsAnonymousType() ?? false)
            {
                var properties = funcResult.GetType().GetProperties();
                foreach (var property in properties)
                {
                    orderByColumns.Add($"{fromTable}.{property.Name}");
                }
            }
            else
            {
                orderby = funcResult?.ToString();
                orderByColumns.Add(orderby);
            }

            orderDirection = descending ? "desc" : "asc";

            return this;
        }

        public SelectQuery GroupBy<T>(Table<T> table, Func<T, object> func, string As = null)
        {
            var tableName = string.IsNullOrEmpty(As) ? table._tableName : As;

            var properties = func(Activator.CreateInstance<T>()).GetType().GetProperties();
            foreach (var property in properties)
            {
                groupedByColumns.Add($"{tableName}.{property.Name}");
            }

            return this;
        }        

        private SelectQuery Compare<T>(CompareType methodType, Table<T> table, Func<T, object> func, object value, object otherValue=null, string As=null)
        {
            string fromTable = (string.IsNullOrEmpty(As) ? table._tableName : As);

            string _value = value?.ToString();

            if (value?.GetType() == typeof(string) || value?.GetType() == typeof(DateTime))
            {
                _value = $"'{value}'";
            }
            else if(value?.GetType() == typeof(System.Collections.IEnumerable))
            {
                _value = string.Join(", ", (System.Collections.IEnumerable)value);
            }
            else if(value?.GetType() == typeof(SelectQuery))
            {
                _value = ((SelectQuery)value).Translate();
            }

            string _otherValue = otherValue?.ToString();
            if (otherValue?.GetType() == typeof(string) || otherValue?.GetType() == typeof(DateTime))
            {
                _otherValue = $"'{otherValue}'";
            }
            else if (otherValue?.GetType() == typeof(System.Collections.IEnumerable))
            {
                _otherValue = string.Join(", ", (System.Collections.IEnumerable)otherValue);
            }
            else if (otherValue?.GetType() == typeof(SelectQuery))
            {
                _otherValue = ((SelectQuery)otherValue).Translate();
            }

            var funcResult = func.Invoke(Activator.CreateInstance<T>());
            string leftSide;

            if (funcResult?.GetType() == typeof(Complex))
            {
                var comp = (funcResult as Complex);
                leftSide = comp.ToString();
            }
            else if (funcResult?.GetType() == typeof(Aggregation))
            {
                var agg = (funcResult as Aggregation);
                agg.baseFromTable = fromTable;
                leftSide = agg.ToString();
            }
            else if (funcResult?.IsAnonymousType() ?? false)
            {
                leftSide =  $"{fromTable}.{funcResult?.GetColumns().First()}";
            }
            else leftSide = funcResult?.ToString();

            var con = new Condition
            {
                CompareType = methodType,
                First = leftSide,
                Second = $"{_value}",
                Third = _otherValue
            };

            if (conditions.Count == 0) conditions.Add(new List<Condition>());
            conditions.Last().Add(con);

            return this;
        }

        private SelectQuery Compare<T1, T2>(CompareType methodType, Table<T1> table1, Func<T1, object> func1, Table<T2> table2, Func<T2, object> func2, string As1 = null, string As2 = null)
        {
            string fromTable1 = (string.IsNullOrEmpty(As1) ? table1._tableName : As1);
            string fromTable2 = (string.IsNullOrEmpty(As2) ? table2._tableName : As2);

            var leftFuncResult = func1.Invoke(Activator.CreateInstance<T1>());
            string leftSide;

            if (leftFuncResult?.GetType() == typeof(Complex))
            {
                var comp = (leftFuncResult as Complex);
                leftSide = comp.ToString();
            }
            else if (leftFuncResult?.GetType() == typeof(Aggregation))
            {
                var agg = (leftFuncResult as Aggregation);
                agg.baseFromTable = fromTable1;
                leftSide = agg.ToString();
            }
            else if (leftFuncResult?.IsAnonymousType() ?? false)
            {
                leftSide = $"{fromTable1}.{leftFuncResult?.GetColumns().First()}";
            }
            else leftSide = leftFuncResult?.ToString();

            var rightFuncResult = func2.Invoke(Activator.CreateInstance<T2>());
            string rightSide;

            if (rightFuncResult?.GetType() == typeof(Complex))
            {
                var comp = (rightFuncResult as Complex);
                rightSide = comp.ToString();
            }
            else if (rightFuncResult?.GetType() == typeof(Aggregation))
            {
                var agg = (rightFuncResult as Aggregation);
                agg.baseFromTable = fromTable2;
                rightSide = agg.ToString();
            }
            else if (rightFuncResult?.IsAnonymousType() ?? false)
            {
                rightSide = $"{fromTable2}.{rightFuncResult?.GetColumns().First()}";
            }
            else rightSide = rightFuncResult?.ToString();

            var con = new Condition
            {
                CompareType = methodType,
                First = leftSide,
                Second = rightSide,
            };

            if (conditions.Count == 0) conditions.Add(new List<Condition>());
            conditions.Last().Add(con);

            return this;
        }

        //Func-Obj
        public SelectQuery IsLess<T>(Table<T> table, Func<T, object> func, object value, string As = null)
        {
            return Compare(CompareType.LessThan, table, func, value, null, As);
        }

        public SelectQuery IsLessOrEqual<T>(Table<T> table, Func<T, object> func, object value, string As = null)
        {
            return Compare(CompareType.LessThanOrEqual, table, func, value, null, As);
        }

        public SelectQuery IsEqual<T>(Table<T> table, Func<T, object> func, object value, string As = null)
        {
            return Compare(CompareType.Equal, table, func, value, null, As);
        }

        public SelectQuery IsBiggerOrEqual<T>(Table<T> table, Func<T, object> func, object value, string As = null)
        {
            return Compare(CompareType.BiggerThanOrEqual, table, func, value, null, As);
        }

        public SelectQuery IsBigger<T>(Table<T> table, Func<T, object> func, object value, string As = null)
        {
            return Compare(CompareType.BiggerThan, table, func, value, null, As);
        }

        public SelectQuery IsNotEqual<T>(Table<T> table, Func<T, object> func, object value, string As = null)
        {
            return Compare(CompareType.NotEqual, table, func, value, null, As);
        }


        //Func-Func
        public SelectQuery IsLess<T1, T2>(Table<T1> table1, Func<T1, object> func1, Table<T2> table2, Func<T2, object> func2, string As1 = null, string As2 = null)
        {
            return Compare(CompareType.LessThan, table1, func1, table2, func2, As1, As2);
        }

        public SelectQuery IsLessOrEqual<T1, T2>(Table<T1> table1, Func<T1, object> func1, Table<T2> table2, Func<T2, object> func2, string As1 = null, string As2 = null)
        {
            return Compare(CompareType.LessThanOrEqual, table1, func1, table2, func2, As1, As2);
        }

        public SelectQuery IsEqual<T1, T2>(Table<T1> table1, Func<T1, object> func1, Table<T2> table2, Func<T2, object> func2, string As1 = null, string As2 = null)
        {
            return Compare(CompareType.Equal, table1, func1, table2, func2, As1, As2);
        }

        public SelectQuery IsBiggerOrEqual<T1, T2>(Table<T1> table1, Func<T1, object> func1, Table<T2> table2, Func<T2, object> func2, string As1 = null, string As2 = null)
        {
            return Compare(CompareType.BiggerThanOrEqual, table1, func1, table2, func2, As1, As2);
        }

        public SelectQuery IsBigger<T1, T2>(Table<T1> table1, Func<T1, object> func1, Table<T2> table2, Func<T2, object> func2, string As1 = null, string As2 = null)
        {
            return Compare(CompareType.BiggerThan, table1, func1, table2, func2, As1, As2);
        }

        public SelectQuery IsNotEqual<T1, T2>(Table<T1> table1, Func<T1, object> func1, Table<T2> table2, Func<T2, object> func2, string As1 = null, string As2 = null)
        {
            return Compare(CompareType.NotEqual, table1, func1, table2, func2, As1, As2);
        }
        

        public SelectQuery In<T>(Table<T> table, Func<T, object> func, object value, string As = null)
        {
            return Compare(CompareType.In, table, func, value, null, As);
        }        
        public SelectQuery Between<T>(Table<T> table, Func<T, object> func, object from, object to, string As = null)
        {
            return Compare(CompareType.Between, table, func, from, to, As);
        }


        //Single format methods
        public SelectQuery Exists(SelectQuery subQuery, string As = null)
        {
            var _value = subQuery.Translate();
            var con = new Condition
            {
                CompareType = CompareType.Exists,
                First = $"{_value}",
            };

            if (conditions.Count == 0) conditions.Add(new List<Condition>());
            conditions.Last().Add(con);

            return this;
        }

        public SelectQuery IsNull<T>(Table<T> table, Func<T, object> func, string As = null)
        {
            return Compare(CompareType.Null, table, func, null, null, As);
        }

        public SelectQuery IsNotNull<T>(Table<T> table, Func<T, object> func, string As = null)
        {
            return Compare(CompareType.NotNull, table, func, null, null, As);
        }


        public SelectQuery And()
        {
            conditions.Add(new List<Condition>());
            return this;
        }

        public SelectQuery Limit(int limit)
        {
            this.limit = limit.ToString();
            return this;
        }

        public SelectQuery Offset(int offset)
        {
            this.offset = offset.ToString();
            return this;
        }

        public string Translate()
        {
            string query = $"SELECT {string.Join(",", selectedColumns)} FROM {string.Join(",", fromTables)}";
            if (conditions.Count > 0)
            {
                query += $" WHERE {string.Join(" AND ", conditions.Select(q => string.Join(" OR ", q)))}";
            }

            if (orderByColumns.Count > 0)
            {
                query += $" ORDER BY {string.Join(",", orderByColumns)} {orderDirection}";
            }

            if (!string.IsNullOrEmpty(limit))
                query += $" LIMIT {limit}";

            if (!string.IsNullOrEmpty(offset))
                query += $" OFFSET {offset}";

            return query;
        }

        public IEnumerable<T> Take<T>()
        {            
            var query = Translate();
            return db.builder._connection.Query<T>(query);            
        }
    }

    public enum CompareType { LessThan, LessThanOrEqual, Equal, BiggerThanOrEqual, BiggerThan, NotEqual, Null, NotNull, Like, In, Exists, Between }
    public class Condition
    {
        public CompareType CompareType { get; set; }
        public string First { get; set; }
        public string Second { get; set; }
        public string Third { get; set; }

        public override string ToString()
        {
            string q = "";
            switch (CompareType)
            {
                case CompareType.LessThan:
                    q = $"{First} < {Second}";
                    break;
                case CompareType.LessThanOrEqual:
                    q = $"{First} <= {Second}";
                    break;
                case CompareType.Equal:
                    q = $"{First} = {Second}";
                    break;
                case CompareType.BiggerThanOrEqual:
                    q = $"{First} >= {Second}";
                    break;
                case CompareType.BiggerThan:
                    q = $"{First} > {Second}";
                    break;
                case CompareType.NotEqual:
                    q = $"{First} <> {Second}";
                    break;
                case CompareType.Null:
                    q = $"{First} IS NULL";
                    break;
                case CompareType.NotNull:
                    q = $"{First} IS NOT NULL";
                    break;
                case CompareType.Like:
                    q = $"{First} LIKE {Second}";
                    break;
                case CompareType.In:
                    q = $"{First} IN ({Second})";
                    break;
                case CompareType.Exists:
                    q = $"EXISTS ({First})";
                    break;
                case CompareType.Between:
                    q = $"{First} BETWEEN {Second} AND {Third}";
                    break;
                default:
                    break;
            }
            return q;
        }
    }
}