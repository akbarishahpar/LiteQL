﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LiteQL
{
    public class Table
    {
        public Builder _builder { get; set; }
        public string _tableName { get; set; }

        protected List<string> _NotNullColumns = new List<string>();
        protected List<string> _primaryKeys = new List<string>();
        protected List<string> _uniqeColumns = new List<string>();
        protected List<string> _autoIncrementColumns = new List<string>();
        protected Dictionary<string, ForeignKey> _foreignKeys = new Dictionary<string, ForeignKey>();
        protected System.Reflection.PropertyInfo[] _properties;

        public Type _tableType { get; set; }

        public Table(Builder builder, string tableName)
        {
            _builder = builder;
            _tableName = tableName;            
        }



        public Table()
        {
            
        }

        public void InitAttributes()
        {
            _properties = _tableType.GetProperties();

            _NotNullColumns = _properties.Where(q => q.CustomAttributes.Any(p => p.AttributeType == typeof(NotNull))).Select(q => q.Name).ToList();
            _primaryKeys = _properties.Where(q => q.CustomAttributes.Any(p => p.AttributeType == typeof(PrimaryKey))).Select(q => q.Name).ToList();
            _uniqeColumns = _properties.Where(q => q.CustomAttributes.Any(p => p.AttributeType == typeof(Unique))).Select(q => q.Name).ToList();
            _autoIncrementColumns = _properties.Where(q => q.CustomAttributes.Any(p => p.AttributeType == typeof(AutoIncrement))).Select(q => q.Name).ToList();
            _properties.ToList().ForEach(property => {
                var fk = property.GetCustomAttributes(typeof(ForeignKey), true).Cast<ForeignKey>().SingleOrDefault();
                if (fk != null)
                {
                    _foreignKeys.Add(property.Name, fk);
                }
            });
        }

        public void Configure()
        {            
            var rows = new List<string>();            
            foreach (var property in _properties)
            {
                
                var cols = new List<string>();
                cols.Add(property.Name);
                cols.Add(_builder.database.GetSqlDataType(property.PropertyType));

                if (_NotNullColumns.Any(q => q == property.Name))
                    cols.Add("NOT NULL");
                

                if (_primaryKeys.Count == 1)
                {
                    if (_primaryKeys.First() == property.Name)
                    {
                        cols.Add("PRIMARY KEY");
                        if (_autoIncrementColumns.Any(q => q == property.Name))
                        {
                            cols.Add("AUTOINCREMENT");
                        }
                    }
                }

                if (_uniqeColumns.Any(q => q == property.Name))
                    cols.Add("UNIQUE");

                rows.Add(string.Join(" ", cols));
            }

            if(_primaryKeys.Count > 1)
                rows.Add($"PRIMARY KEY ({string.Join(", ", _primaryKeys)})");

            if (_foreignKeys != null)
            {
                foreach (KeyValuePair<string, ForeignKey> fk in _foreignKeys)
                {
                    rows.Add($"FOREIGN KEY({fk.Key}) REFERENCES {fk.Value.RefrenceTable}({fk.Value.RefrenceColumn})");
                }
            }

            string query = $"CREATE TABLE IF NOT EXISTS {_tableName} ({string.Join(",", rows)})";
            _builder._connection.Execute(query);
        }
    }
    public class Table<T> : Table
    {
        public Table(Builder builder, string tableName)
            : base(builder, tableName)
        {
            _tableType = typeof(T);
        }

        public Table():base()
        {

        }

        public void Insert(T instance)
        {            
            var properties = _properties.Where(q => _autoIncrementColumns.All(p => p != q.Name)).ToArray();

            var columns = string.Join(", ", properties.Select(q => q.Name));
            var values = string.Join(", ", properties.Select(q =>
            {
                var value = q.GetValue(instance);
                if (q.PropertyType == typeof(string)) return $"'{value}'";
                else if (q.PropertyType == typeof(DateTime)) return $"'{((DateTime)value).ToFormattedString()}'";
                return value;
            }));

            string query = $"insert into {_tableName} ({columns}) values ({values})";
            _builder._connection.Execute(query);
        }

        public void Insert(IEnumerable<T> instances)
        {
            List<string> queries = new List<string>();

            var properties = _properties.Where(q => _autoIncrementColumns.All(p => p != q.Name)).ToArray();

            var columns = string.Join(", ", properties.Select(q => q.Name));
            string values;
            List<string> valuesList = new List<string>();
            foreach (var instance in instances)
            {
                values = string.Join(", ", properties.Select(q =>
                {
                    var value = q.GetValue(instance);
                    if (q.PropertyType == typeof(string)) return $"'{value}'";
                    else if (q.PropertyType == typeof(DateTime)) return $"'{((DateTime)value).ToFormattedString()}'";
                    return value;
                }));
                values = $"({values})";
                valuesList.Add(values);
            }

            values = string.Join(",", valuesList);
            string query = $"insert into {_tableName} ({columns}) values {values}";
            _builder._connection.Execute(query);
        }

        public void Update(T instance, string where)
        {
            var values = string.Join(", ", _tableType.GetProperties().Select(q =>
            {
                var value = q.GetValue(instance);
                if (q.PropertyType == typeof(string)) value = $"'{value}'";
                return $"{q.Name} = {value}";
            }));

            string query = $"update {_tableName} set {values}";

            if (!string.IsNullOrEmpty(where))
                query = $"{query} where {where}";

            _builder._connection.Execute(query);
        }

        public void Update(T instance)
        {
            string where = string.Join(" and ", _primaryKeys.Select(q =>
            {
                var property = _tableType.GetProperty(q);
                var value = property.GetValue(instance);
                if (property.PropertyType == typeof(string)) value = $"'{value}'";
                return $"{q} = {value}";
            }));

            Update(instance, where);
        }

        public void Delete(string where = null)
        {
            string query = $"delete from {_tableName}";
            if (!string.IsNullOrEmpty(where))
                query = $"{query} where {where}";

            _builder._connection.Execute(query);
        }
        public void Delete(T instance)
        {
            string where = string.Join(" and ", _primaryKeys.Select(q =>
            {
                var property = _tableType.GetProperty(q);
                var value = property.GetValue(instance);
                if (property.PropertyType == typeof(string)) value = $"'{value}'";
                return $"{q} = {value}";
            }));
            Delete(where);
        }
        public void Delete(IEnumerable<T> instances)
        {
            List<string> wheres = new List<string>();
            foreach (var instance in instances)
            {
                string where = string.Join(" and ", _primaryKeys.Select(q =>
                {
                    var property = _tableType.GetProperty(q);
                    var value = property.GetValue(instance);
                    if (property.PropertyType == typeof(string)) value = $"'{value}'";
                    return $"{q} = {value}";
                }));

                where = $"({where})";
                wheres.Add(where);
            }

            var ors = string.Join(" or ", wheres);

            Delete(ors);
        }

    }
}
