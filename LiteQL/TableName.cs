﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LiteQL
{
    public class TableName : Attribute
    {
        public string Name { get; set; }
        public TableName(string Name)
        {
            this.Name = Name;
        }
        
    }
}
