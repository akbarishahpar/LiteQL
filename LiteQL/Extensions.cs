﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL
{
    public static class Extensions
    {
        public static bool IsAnonymousType(this object obj)
        {
            var type = obj.GetType();

            bool hasCompilerGeneratedAttribute = type.GetCustomAttributes(typeof(CompilerGeneratedAttribute), false).Count() > 0;
            bool nameContainsAnonymousType = type.FullName.Contains("AnonymousType");
            bool isAnonymousType = hasCompilerGeneratedAttribute && nameContainsAnonymousType;

            return isAnonymousType;
        }

        public static List<string> GetColumns(this object obj)
        {
            if (!obj.IsAnonymousType()) throw new Exception("Only anonymous types are acceptable");
            var properties = obj.GetType().GetProperties();
            return properties.Select(q => q.Name).ToList();
        }

        public static string Translate(this object obj, string fromTable)
        {
            var columns = obj.GetColumns();
            if (columns.Count != 1) throw new Exception("Only one column is acceptable");
            return $"{fromTable}.{columns.First()}";
        }

        public static string Translate(this object obj, Table fromTable)
        {
            var columns = obj.GetColumns();
            if (columns.Count != 1) throw new Exception("Only one column is acceptable");
            var s = $"{fromTable._tableName}.{columns.First()}";
            return s;
        }

        public static string Translate(this object obj)
        {
            var columns = obj.GetColumns();
            if (columns.Count != 1) throw new Exception("Only one column is acceptable");
            return columns.First();
        }

        public static Complex Complex(this string s)
        {
            return new Complex(s);
        }

        public static string DateTimeFormat = "MM/dd/yyyy HH:mm:ss.fff";
        public static string ToFormattedString(this DateTime d)
        {
            return d.ToString(DateTimeFormat);
        }

        public static DateTime FromFormattedDateTime(this string s)
        {
            return DateTime.ParseExact(s, DateTimeFormat, null);
        }
    }
}
