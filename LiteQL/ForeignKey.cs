﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LiteQL
{
    public class ForeignKey : Attribute
    {
        public string RefrenceTable { get; set; }
        public string RefrenceColumn { get; set; }
        public ForeignKey(string RefrencTable, string RefrenceColumn)
        {
            this.RefrenceTable = RefrencTable;
            this.RefrenceColumn = RefrenceColumn;
        }
    }
}
