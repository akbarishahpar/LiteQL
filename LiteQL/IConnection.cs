﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LiteQL
{
    public interface ISQLiteConnection
    {        
        IEnumerable<T> Query<T>(string query);
        int Execute(string query);
    }
}
