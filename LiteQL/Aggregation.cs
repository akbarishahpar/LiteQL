﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL
{
    public enum AggregationFunction { Count, Min, Max, Sum, Total, Average, GroupConcat }
    public class Aggregation
    {
        AggregationFunction function;
        string x;
        string y;
        public string baseFromTable;
        public string secondaryFromTable;

        public Aggregation(AggregationFunction function, string x, string y)
        {
            this.function = function;
            this.x = x;
            this.y = y;            
        }

        public static Aggregation Count()
        {
            return new Aggregation(AggregationFunction.Count, null, null);
        }

        public static Aggregation CountOf()
        {
            return new Aggregation(AggregationFunction.Count, "*", null);
        }

        public static Aggregation CountOf(object o)
        {            
            var property = o.GetType().GetProperties().First();
            return new Aggregation(AggregationFunction.Count, property.Name, null);
        }        
        
        public static Aggregation MaxOf(object o)
        {
            var x = o.IsAnonymousType() ? o.GetColumns().First() : o.ToString();
            return new Aggregation(AggregationFunction.Max, x, null);
        }

        public static Aggregation MinOf(object o)
        {
            var x = o.IsAnonymousType() ? o.GetColumns().First() : o.ToString();
            return new Aggregation(AggregationFunction.Min, x, null);
        }

        public static Aggregation SumOf(object o)
        {
            var x = o.IsAnonymousType() ? o.GetColumns().First() : o.ToString();
            return new Aggregation(AggregationFunction.Sum, x, null);
        }

        public static Aggregation TotalOf(object o)
        {
            var x = o.IsAnonymousType() ? o.GetColumns().First() : o.ToString();
            return new Aggregation(AggregationFunction.Total, x, null);
        }

        public static Aggregation AverageOf(object o)
        {
            var x = o.IsAnonymousType() ? o.GetColumns().First() : o.ToString();
            return new Aggregation(AggregationFunction.Average, x, null);
        }

        public static Aggregation GroupConcatOf(object o, Table secondaryTable=null)
        {
            var first = o.GetType().GetProperties().First();
            var second = o.GetType().GetProperties().LastOrDefault();            

            var agg = new Aggregation(AggregationFunction.Max, first.Name, second?.Name);

            if (secondaryTable != null)
            {
                agg.secondaryFromTable = secondaryTable._tableName;
            }

            return agg;
        }

        public override string ToString()
        {
            var _baseFromTable = string.IsNullOrEmpty(baseFromTable) ? "" : $"{baseFromTable}.";
            var _secondaryFromTable = string.IsNullOrEmpty(secondaryFromTable) ? "" : $"{secondaryFromTable}.";

            switch (this.function)
            {
                case AggregationFunction.Count:
                    {
                        if (string.IsNullOrEmpty(x)) return "COUNT(*)";
                        else return $"COUNT({baseFromTable}.{x})";                        
                    }
                case AggregationFunction.Min: return $"MIN({_baseFromTable}{x})";
                case AggregationFunction.Max: return $"MAX({_baseFromTable}{x})";
                case AggregationFunction.Sum: return $"SUM({_baseFromTable}{x})";
                case AggregationFunction.Total: return $"TOTAL({_baseFromTable}{x})";
                case AggregationFunction.Average: return $"AVERAGE({_baseFromTable}{x})";
                case AggregationFunction.GroupConcat:
                    {
                        if (!string.IsNullOrEmpty(x) && !string.IsNullOrEmpty(y)) return $"GROUP_CONCAT({_baseFromTable}{x}, {_secondaryFromTable}{y})";
                        else if (!string.IsNullOrEmpty(x)) return $"GROUP_CONCAT({_baseFromTable}{x})";
                        break;
                    }
                default:
                    break;
            }

            throw new Exception("Undefiend function");
        }
    }
}
