﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteQL
{
    public class Complex
    {
        string data;
        public Complex(string data)
        {
            this.data = data;
        }

        public override string ToString()
        {
            return data;
        }
    }
}
